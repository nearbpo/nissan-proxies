# NISSAN API - Reference implementation for CI

This is a reference implementation that leverages apigee-deploy-maven-plugin and Apickli for CI and BDD testing.

## Proxy Deployment & Tests

```
 mvn install -P<environment> -Dusername=<apigeeEnvDeploymentUser> -Dpassword=<password>
```