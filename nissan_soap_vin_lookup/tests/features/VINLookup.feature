Feature:
	As a Nissan client I want to verify a vehile VIN number for data correlation purposes.

	Scenario: Setting headers in GET request
		Given I set User-Agent header to apickli
        And I set body to <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:veh="http://www.nissanusa.com/VehicleInformation/" xmlns:xsd="http://webservices.vehicleinfo.com/xsd"><soapenv:Header><wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:UsernameToken wsu:Id="UsernameToken-AC3EDB77A97878227C15171908709541"><wsse:Username>NNA-SVC-002</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">Nissan123</wsse:Password><wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">NJ5exS6F85ScKgV36F8fzw==</wsse:Nonce><wsu:Created>2018-01-29T01:54:30.950Z</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><veh:getVehicleInformation><!--Optional:--><veh:regParams><xsd:VIN>JN8AF5MR1HT704179</xsd:VIN><!--Optional:--><!-- <xsd:lookupOpt>?</xsd:lookupOpt> --></veh:regParams></veh:getVehicleInformation></soapenv:Body></soapenv:Envelope>
        And I set Content-Type header to text/xml
        And I set SOAPAction header to "urn:getVehicleInformation"
		When I POST to /nissan/vehicleLookup
        Then response code should be 200
        And response body should be valid xml